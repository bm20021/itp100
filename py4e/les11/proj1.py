lis = [10, 20, 50, 100, 200, 500, 1000]
serials = ['A201550BBB', 'XYZ200019Z', 'ABC1800910', 'ERF200220', 'GHJ1900500J', 'XYZ20191000C', 'XYZ20191100', '234GJF234']
def currencypython():
    for x in serials:
        if len(x) >= 10 and len(x) <= 12 and x[1] != x[2] != x[3]:
            if x[0:3] == x[0:3].upper() and x[0:3].isalpha() == True:
                if int(x[3:7]) >= 1900 and int(x[3:7]) <= 2019:
                    if x[-1:] == x[-1:].upper() and x[-1:].isalpha() is True:
                        for value in lis:
                            if len(x) == 10 and int(x[7:9]) == value:
                                print(x, 'this is a valid serial number')
                                break
                            elif len(x) == 11 and int(x[7:10]) == value:
                                print(x, 'this is a valid serial number')
                                break
                            elif len(x) == 12 and int(x[7:11]) == value:
                                print(x, 'this is a valid serial number')
                                break
                            elif value == 1000 and len(x) != 12 and x[7:11] != value:
                                print(x, 'failed denomination')

                            else:
                                continue
                    else:
                        print(x, 'failed last character good')
                    '''if x[-1:].isalpha() is True:
                        print(x, 'upper alpha good')
                    else:
                        print(x, 'failed upper alphs')'''
                else:
                    print(x, 'failed year')
            else:
                print(x, 'failed letters')
        else:
            print(x, 'failed total number')
'''
                        for value in list:
                            if len(x) == 10:
                                if int(x[7:9]) == value:
                                    x
                            if len(x) == 11:
                                if int(x[7:10]) == value:
                                    x
                            if len(x) == 12:
                                if int(x[7:11]) == value:
                                    x
'''
#Code below is split up into multiple regular expressions to help show the code is correct and it is eliminating error serial numbers with the right causes

#This code works as so far as tested, however do not know of a way with regular expression to signfify each of the first three values need to be "distinct" or different as disscussed in class
def currencyregex():
    serial = ['A201550BBB', 'XYZ200019Z', 'ABC1800910', 'ERF200220', 'GHJ1900500J', 'XYZ20191000C', 'XYZ20191000', 'ABBGJF234']
    import re
    for x in serial:
        #don't need a part of code to test whether 10-12 letters as if it is not it will fail somewhere in other statements
        #if statement below gets 3 uppercase alphabetical letters but not distinct in the sense that that means they are all different.
        char1 = re.findall('^([A-Z])', x)
        char2 = re.findall('^.([A-Z])', x)
        char3 = re.findall('^..([A-Z])', x)
        #print(char1, char2, char3)
        if char1 != char2 != char3:
            if re.findall('^...[1-2][0, 9][0-9][0-9]', x):
                if re.findall('^.......[1, 2, 5]0[A-Z]', x) or re.findall('^.......[1, 2, 5]00[A-Z]',x) or re.findall('^.......1000[A-Z]',x):
                    print(x, 'this is a valid currency!!!')
                else:
                    print(x, 'failed currency denomination or last letter')
            else:
                print(x, 'failed year') 
        else:
            print(x, 'first 3 letters are not all distinct or not letters')



print('output with just python code below')   
currencypython()
print('')
print('')
print('output with python with reg express code below')
currencyregex()