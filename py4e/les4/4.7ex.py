def computegrade(score):
    
    if score > 0 and score <= 1.0:
        if score >= .9:
            print("A")
        elif score >= .8:
            print("B")
        elif score >= .7:
            print("C")
        elif score >= .6:
            print("D")
        elif score >= .5:
            print("E")
        elif score < .6:
            print("F")
    else:
        print("Bad Score")

try:
    Score = float(input('Enter Score: '))
    computegrade(Score)
except:
    print("Bad Score")
