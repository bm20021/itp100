openmail = open(input('Enter file name: '))
messages = dict()

for line in openmail:
  if line.startswith('From ') is True:
    word = line.split()
    word2 = word[1]
    if word2 not in messages:
      messages[word2] = 1
    else:
      messages[word2] += 1

print(messages)
