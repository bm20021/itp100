fhand = open('mbox-short.txt')
messages = dict()
for line in fhand:
    if line.startswith('From '):
        address = line.split()
        word2 = address[1]
        #print(word2)
        if word2 not in messages:
            messages[word2] = 1
        else:
            messages[word2] += 1

mostcommits = list()
keys = messages.keys()
vals = messages.values()

for (key, val) in list(messages.items()):
   mostcommits.append((val, key))
#print(mostcommits)

mostcommits.sort(reverse=True)
for key, val in mostcommits[:1]:
    print(val, key)
