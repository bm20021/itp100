fhand = open('mbox-short.txt')
timeday = dict()
lst = list()
for line in fhand:
    if line.startswith('From '):
        s = line.split()
        d = s[5]
        split5 = d.split(':')
        hour = split5[0]
        #print(hour)
        if hour not in timeday:
            timeday[hour] = 1
        else:
            timeday[hour] += 1
print(timeday)

for key, val in timeday.items():
    lst.append((key, val))

lst.sort()

for key, val in lst:
    print(key, val)