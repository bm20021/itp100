try:
    score = float(input('Enter Score: '))
    if score > 0 and score <= 1.0:
        if score >= .9:
            print("A")
        if score >= .8 and score < .9:
            print("B")
        if score >= .7 and score < .8:
            print("C")
        if score >= .6 and score < .7:
            print("D")
        if score < .6 and score < .7:
            print("F")
    else:
        print("Bad Score")
except:
    print("Bad score")

