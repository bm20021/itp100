userfileinput = input("Input your file name within this directory here: ")
userfile = userfileinput.strip()

try:
  fh = open(userfile)
  x = []
  for lx in fh:
    ly = lx.rstrip()
    if not lx.startswith('X-DSPAM-Confidence:'):
      continue
    split = lx.split()
    juice = split[1]
    fire = x.append(juice)
  firey = int(len(x))
  print('the number of lines with spam confidence info is:', firey)
  floats = [float(x) for x in x]
  print(floats)
  sum_x_list = sum(floats)
  print('the total of the spam confidence values is:', sum_x_list)
  mean = sum_x_list/firey
  print('The mean spam confidence value is:', mean)

except:
  print('Unfortunately, the file name inputted is invalid.  Make sure you have put in file name that is a real file in the current working diretory.')
