#Do the following exercise and show it during the class: Code a python script to [0] open myfiles.txt file, [a] select all lines containing pdf/txt/doc/ppt etc. for three kinds of files, each kind up to 10 files. [b] count how many directories are there and list these directories (the lines begin with a 'd', [c] display all "invisible" files that begin with a dot "." (when you do "ls" only, without " -al" this is what you don't see) [d] which file is the biggest (the number right before month "Oct", "Sep" etc). [e] How many lines is there in this file? Complete this project by the end of next chapter "Lists" then present on R 10/28.
#juice = open('myfiles.txt', 'r')

def ansA1():
  juice = open('myfiles.txt', 'r')
  a1 = 0
  for x in juice:
    x = x.strip()
    if "pdf" not in x:
      continue
    a1 = a1 + 1
    if a1 <= 10:
      print(x)
  print('a) The total number of pdf files is: ', a1)
  print('a) 10 (or if the total number is less than 10, then those are print    ed) of these pdf files are printed above')
  print('')

def ansA2():
  juice = open('myfiles.txt', 'r')
  a2 = 0
  for x in juice:
    x = x.strip()
    if ".html" not in x:
      continue
    a2 = a2 + 1
    if a2 <= 10:
      print(x)
  print('a) The total number of html files is: ', a2)
  print('a) 10 (or if the total number is less than 10, then those are print    ed) of these html files are printed above')
  print('') 

def ansA3():
  juice = open('myfiles.txt', 'r')
  a3 = 0
  for x in juice:
    x = x.strip()
    if "pptx" not in x:
      continue
    a3 = a3 + 1
    if a3 <= 10:
      print(x)
  print('a) The total number of pptx files is: ', a3)
  print('a) 10 (or if the total number is less than 10, then those are printed) of these pptx files are printed above')
  print('')

def ansB():
  juice = open('myfiles.txt', 'r')
  d = 0
  jump = []
  for x in juice:
    #  x = x.strip()
    if x.startswith('d'):
      d = d + 1
      lax = jump.append(x)
  print('b) The number of directories is:', d)
  print('')
  print('b) The lines of these directories are:', jump)
  print('')

def ansC():
  juice = open('myfiles.txt', 'r')
  xx = []
  invisible = 0
  for x in juice:
    x = x.rstrip()
    files = x.split()
    if ' .' not in x:
      continue
    invisible = invisible + 1
    xx.append(x)
  print('c) Here are all invisible files and the total number of all of them    :',xx)
  print('')
  print('c) the total number of invisible files/directories is ',invisible)
  print('')

def ansD():
  juice = open('myfiles.txt', 'r')
  yolo = 0.0
  xy = []
  count = 0
  n = 9
  for x in juice:
    x = x.strip()
    files = x.split()
    sx = str('x')
    for sx in x:
      count = count + 1
    if count < 15:
      continue
    filess = (files) [4]
    filesss = float(filess)
    #print(filesss)
    if filesss > yolo:
      yolo = filesss
    for fileness in x:
      dimple = x
  print('d) The file that is the biggest has: ', yolo, "bites of data. ", "    The line of this file is: ", dimple )
  print('')
  

def ansE():
  juice = open('myfiles.txt', 'r')
  y = 0
  for x in juice:
    x = x.strip()
    y = y + 1
  print('e) The total number of files in this directory ', y)
  print('')

ansA1()
ansA2()
ansA3()
ansB()
ansC()
ansD()
ansE()
