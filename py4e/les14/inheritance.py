from createobject import BestGroup

class PythonClass(BestGroup):
    classmembers = 0
    def six(self):
        self.classmembers += 6
        self.party()
        print(self.name,"classmembers",self.classmembers)

s = BestGroup("Bradley")
s.party()
j = PythonClass("Kayden")
j.party()
j.six()
print(dir(j))
