fhand = open('8.2ex.txt')
count = 0
for line in fhand:
  words = line.split()
  print('Debug:', words)
  
  #The line below this/the end result line is not properly guarded as at the end it is printing the 3rd word and the program quits only if the # of words is 0 but what if there were only two words?
  if len(words) == 0:
    continue
  if words[0] != 'From':
    continue
  print(words[2])
