'''<2> This one is easier: program a user interface similar to <1> to do the inverse function. 
(i) input: Ask the user to input a base and a number, for either Binary, Octal, or Hexadecimal. 
(ii) output: Then use corresponding weight tables of Binary, Octal, or Hexadecimal to construct its 
equivalent decimal number and print it.'''


num = input('Input a valid number with its base (only works for based 2, 8, and 16) in the format "num, base" to convert it to decimal (make sure you input the right number and base or you may experience an error):')

if ', ' not in num:
    print('your input is not correctly formatted, please run the code again')
    quit()
words = num.split(', ')
number = words[0]
base = words[1]
print(number)
print(base)
numdigits = list(number)
print(numdigits)
if int(base) == 16:
    dec = []
    hexdigits = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"]
    for digit in numdigits:
        digit = hexdigits.index(digit)
        #print(digit)
        #print(digits)
        dec.append(digit)
    #print(dec)
    dec.reverse()
    #print(dec)
    total = 0
    for x in dec:
        #print(x)
        x = int(x)
        location = dec.index(int(x))
        #print(location)
        decimal = x * 16**((int(location)))
        #print(decimal)
        total += decimal
    print('decimal form is', total)
if int(base) == 8:
    octaldigits = ["0", "1", "2", "3", "4", "5", "6", "7", "10", "11", "12", "13", "14", "15", "16", "17"]
    dec = []
    for digit in numdigits:
        digit = octaldigits.index(digit)
        #print(digit)
        #print(digits)
        dec.append(digit)
    #print(dec)
    dec.reverse()
    #print(dec)
    total = 0
    for x in dec:
        #print(x)
        x = int(x)
        location = dec.index(int(x))
        #print(location)
        decimal = x * 8**((int(location)))
        #print(decimal)
        total += decimal
    print('decimal form is', total)
if int(base) == 2:
    bindigits = ["0", "1"]
    dec = []
    for digit in numdigits:
        digit = bindigits.index(digit)
        #print(digit)
        #print(digits)
        dec.append(digit)
    #print(dec)
    dec.reverse()
    #print(dec)
    total = 0
    for x in dec:
        #print(x)
        x = int(x)
        location = dec.index(int(x))
        #print(location)
        decimal = x * 2**((int(location)))
        #print(decimal)
        total += decimal
    print('decimal form is', total)


