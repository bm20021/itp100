#Change the urllinks.py program to extract and count paragraph (p) tags from the retrieved HTML document and 
# display the count of the paragraphs as the output of your program. 
# Do not display the paragraph text, only count them. 
# Test your program on several small web pages as well as some larger web pages.

#This program doesn't output at all for some websites, and for others it has errors: assume this is because of broken html/coding that is
#specifically designed to block this sort of thing. Also one error (possibly)is that it will count all "paragraphs" even if they are 
# commented out in 
#the code (so they won't render if one is actually viewing the webpage)


# To run this, download the BeautifulSoup zip file
# http://www.py4e.com/code3/bs4.zip
# and unzip it in the same directory as this file

import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import ssl

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

url = input('Enter - ')
html = urllib.request.urlopen(url, context=ctx).read()
soup = BeautifulSoup(html, 'html.parser')

# Retrieve all of the anchor tags
tags = soup('p')
num = 0
for tag in tags:
    num += 1
    #print(tag.get('p', None))

print('The total number of paragraphs in this html documnet is: ', num)

#http://data.pr4e.org/romeo.txt
#http://www.py4inf.com/code/mbox-short.txt