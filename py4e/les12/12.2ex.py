#Change your socket program so that it counts the number of characters it has received and stops displaying any text 
# after it has shown 3000 characters. The program should retrieve the entire document and count the total number of characters 
# and display the count of the number of characters at the end of the document.

#Important Note:  This type of program seems to work only for HTTP protocol - I guess HTTPS is different?  Anyway it works just fine for the
#text file that Chuck uses (the romeo one) but has problems with some of the characters in his jpeg image file.  So this program is optimized
# for plain-text HTTP files it seems.  Also it counts characters that aren't apparently seen - but hopefully it is supposed to do this?

import socket
import time 

try:
    user = input('Enter a valid url: ')
    spliter = user.split('/')
    hostname = spliter[2]
    print(hostname)
    mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    mysock.connect((hostname, 80))
    #string = str('GET', user, 'HTTP/1.0\r\n\r\n')
    cd = str(user)
    cf = 'GET '
    cg = ' HTTP/1.0\r\n\r\n'
    combo = str(cf + cd + cg)
    print(combo)
    cmd = combo.encode()
    #mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
    #mysock.connect(('data.pr4e.org', 80))
    #cmd = 'GET http://data.pr4e.org/romeo.txt HTTP/1.0\r\n\r\n'.encode()
    #mysock.send(cmd)
    mysock.send(cmd)
    num = 0
    print(num)
    while True:
        try:
            data = mysock.recv(1)
            if len(data) < 1:
                break
            #time.sleep(0.25)
            num += len(data)
            if num <= 3001:
                print(len(data), num)
                print(data.decode(),end='')
        except:
            print('\r\n')
            continue

    mysock.close()

    print('The total number of characters is (including spaces): ', num)

except:
    print('the url inputted is improperly formatted or non-exsistent')

