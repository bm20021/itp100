#Exercise 1: Change the socket program socket1.py to prompt the user for the URL so it can read any web page. 
# You can use split('/') to break the URL into its component parts so you can extract the host name for the socket connect call. 
# Add error checking using try and except to handle the condition where the user enters an improperly formatted or non-existent URL.

import socket
try:
    user = input('Enter a valid url: ')
    spliter = user.split('/')
    hostname = spliter[2]
    print(hostname)
    mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    mysock.connect((hostname, 80))
    #string = str('GET', user, 'HTTP/1.0\r\n\r\n')
    cd = str(user)
    cf = 'GET '
    cg = ' HTTP/1.0\r\n\r\n'
    combo = str(cf + cd + cg)
    print(combo)
    cmd = combo.encode()
    #mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
    #mysock.connect(('data.pr4e.org', 80))
    #cmd = 'GET http://data.pr4e.org/romeo.txt HTTP/1.0\r\n\r\n'.encode()
    #mysock.send(cmd)
    mysock.send(cmd)

    while True:
        data = mysock.recv(512)
        if len(data) < 1:
            break
        print(data.decode(),end='')

    mysock.close()

except:
    print('the url inputted is improperly formatted or non-exsistent')