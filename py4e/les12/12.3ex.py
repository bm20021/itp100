#Exercise 3: Use urllib to replicate the previous exercise of (1) retrieving the document from a URL, 
# (2) displaying up to 3000 characters, and (3) counting the overall number of characters in the document. 
# Don’t worry about the headers for this exercise, simply show the first 3000 characters of the document contents.\

#http://data.pr4e.org/romeo.txt
#http://www.py4inf.com/code/mbox-short.txt

import urllib.request, urllib.parse, urllib.error

document = input('Input a valid url here: ')
fhand = urllib.request.urlopen(document)
num = 0
while True:
    char = fhand.read(1)         
    if not char:
        break   
    num += 1 
    if num <= 3000:
        print(char)

print('The total number of characters is (including spaces): ', num)


