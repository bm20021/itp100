#This program gets all the links off of the homepage of dictionary.com and then gets all the links (that it can without problems, 
# in which case it displays somesort of error) from the pages that the links on the homepage go to.  
# The rendering is much easier to see in the terminal than the results file  - 
# but this would likely be remedied with some more editing on the openfile.write() things which write the info to the file.

openfile = open('results.txt', 'w+')

#from _typeshed import NoneType
#from _typeshed import NoneType
import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import ssl

#Ignore SSl certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE
url = 'https://www.dictionary.com/'
html = urllib.request.urlopen(url, context=ctx).read()
soup = BeautifulSoup(html, 'html.parser')

#retrieve all anchor tags
tags = soup('a')

string = str(tags)
lists = []
for tag in tags:
    try:
        fire = tag.get('href', None)
        if fire != None and str(fire.startswith('http')):
            #print(fire)
        #Statement above prints all links on the dictionary.com page
        #Statements below write the links, and give each one a line in the output file known as results.txt
            openfile.write("\n\n")
            openfile.write(fire)
            lists.append(fire)
            #Statements below open each link and both print the first letter in the terminal and print it to the results file
    except:
        print('error with one of the links')
        openfile.write('\nerror with current link')
        continue
print(lists)
for x in lists:
    print(x)
    openfile.write(x)
    try:
        link = urllib.request.urlopen(str(x), context=ctx).read()
        soup2 = BeautifulSoup(link, 'html.parser')
        header = soup2('a')
        try:
            for head in header:
                water = head.get('href', None)
                '''print('           ', water)
                openfile.write('        ')
                openfile.write(str(water))'''
                if water != None and str(water.startswith('http')):
                    print('        ', water)
                    openfile.write('        ')
                    openfile.write(str(water))
                    openfile.write('\n')
                else:
                    print('problem if statement', water)
                    openfile.write('   problem with link - wrong format or something  ')
                    openfile.write(str(water))
                    openfile.write('\n')
                    continue
        except:
            print('       error with souping', water)
            openfile.write('\n          error with souping link ')
            openfile.write(str(water))
            continue
    except:
        print('        problem with link', water)
        openfile.write('\nproblem with link parsing        ')
        openfile.write(str(water))
        openfile.write('\n')
        continue

    '''except:
                print('error with one of the links')
                openfile.write('\nerror with current link')'''
    '''print('bad if')
        openfile.write('bad if')
                print('error with this link that is from the page from the link on the homepage')
                openfile.write('\nerror with one of the child links that is from the page from the link above on the homepage')
                continue'''
    
       #except:
         #   continue
        #openfile.write(x[1])
        #openfile.write(x[1])

    